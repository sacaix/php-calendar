<?php
// Calendari°° PHP // Functions
// Problem Y2K38 solved!
// arnAu bellavista
// @sacaix
// kipple.net

class Calendar00{
	public $year;
	public $month;
	public $day;
	public $data;
	public $week;
	public $nav;
	public $path;
	public $lang;
	
	function set_year($year){
		$this->year = $year;
	}
		function get_year(){
			return $this->year;
		}
	function set_month($month){
		$this->month = $month;
	}
	function set_day($day){
		$this->day = $day;
	}
	function set_week($week){
		$this->week = $week;
	}
	function set_nav($nav){
		$this->nav = $nav;
	}
	function set_path($path){
		$this->path = $path;
	}
	function set_lang($lang){
		$this->lang = $lang;
	}
	
	// Agafem l'any i mes actual si no estan definits
	function __construct(){
		$this->data = date("Y-n-j"); // Agafem la data actual com a data base // date("Y-m-d H:i:s");
		list($a,$m,$d) = explode('-',$this->data);
		if(empty($this->year)){
			$this->year = $a;
		}
		if(empty($this->month)){
			$this->month = $m;
		}
	}
	
	// Mostrar un mes
	function month(){
		return $this->mostra($this->year, $this->month, $this->day);
	}
	
	// Mostrar l'any sencer
	function year(){
		$anySencer = "";
		for($i=1; $i<=12; $i++){
			$anySencer .= $this->mostra($this->year, $i, '');
		}
		return $anySencer;
	}
	
	// Mostrar l'any sencer
	function months($m){
		$mesos = "";
		$mAny = $this->year;
		$mMes = $this->month;
		for($i=0; $i<$m; $i++){
			$mesos .= $this->mostra($mAny, $mMes, '');
			if($mMes==12){
				$mAny++;
				$mMes = 1;
			}else{
				$mMes++;
			}
		}
		return $mesos;
	}
	
	function mostra($anyA,$mesA,$diaA){
		$dataSelec = $anyA."-".$mesA."-".$diaA;
		// Dades del mes
		$data_1r_mes = $anyA."-".$mesA."-1";
		// Mirem quin dia de la setmana comença el mes
		$dia_setmana_inici = diaDeLaSetmana($data_1r_mes);
		// Guardem el nombre de dies que te el mes
		$dies_del_mes = numeroDiesMes($mesA,$anyA);
		
		// Calculem quantes setmanes te el mes
		$dies_primera_setmana_falta = $dia_setmana_inici - 1; // Calculem quants dies hem de sumar a la primera setmana perquè faci 7
		$dies_del_mes_total = $dies_del_mes + $dies_primera_setmana_falta; // Sumem els dies del mes + els dies que falten a la primera setmana
		$setmanes_mes = ceil($dies_del_mes_total / 7); // Calculem quantes setmanes hi ha // ceil() Arrodonim cap amunt
		
		$cal00 = "<table class='calendari'>";
		
			$cal00 .= "<tr class='navegacio'>";
				$cal00 .= "<th colspan='7'>";
				$cal00 .= "<div>";
					if($this->nav){
						// Navegació entre mesos i anys
						if(empty($this->path)){
							$this->path = '?data=';
						}
						$anyAa = $anyA-1; // Any anterior
						$cal00 .= "<div class='fletxes anterior'><strong>";
							$cal00 .= "<a href='".$this->path.$anyAa."-".$mesA."' title='Any anterior'>&#10094;&#10094;</a>"; // << Any anterior
							// Anar al mes anterior
							if($mesA > 1){
								$mesAa = $mesA-1; // Mes anterior
								$cal00 .= "<a href='".$this->path.$anyA."-".$mesAa."' title='Mes anterior'>&#10094;</a>"; // < Mes anterior
							}else{ // Si és Gener canvia a Desembre de l'any anterior
								$cal00 .= "<a href='".$this->path.$anyAa."-12' title='Mes anterior'>&#10094;</a>"; // < Mes anterior
							}
						$cal00 .= "</strong></div>";
					}
					$cal00 .= "<div class='fletxes'>";
					$cal00 .= nomMes($mesA-1,$this->lang).' '.$anyA;
					$cal00 .= "</div>";
					
					if($this->nav){
						// Anar a l'any anterior
						$anyAp = $anyA+1; // Any posterior
						$cal00 .= "<div class='fletxes posterior'><strong>";
							// Anar al mes següent
							if($mesA < 12){
								$mesAp = $mesA+1; // Mes posterior
								$cal00 .= "<a href='".$this->path.$anyA."-".$mesAp."' title='Mes següent'>&#10095;</a>"; // > Mes següent
							}else{ // Si és Desembre canvia a Gener de l'any següent
								$cal00 .= "<a href='".$this->path.$anyAp."-01' title='Mes Següent'>&#10095;</a>"; // > Mes següent
							}
							// Anar a l'any següent
							$cal00 .= "<a href='".$this->path.$anyAp."-".$mesA."' title='Any següent'>&#10095;&#10095;</a>"; // Any següent
						$cal00 .= "</strong></div>";
					}
					$cal00 .= "</div>";
				$cal00 .= "</th>";
			$cal00 .= "</tr>";
			
			if($this->week){
				// Fila amb el nom dels dies de la setmana
				$cal00 .= "<tr id='dies_setmana'>";
					for($i=0; $i<7; $i++){
						$cal00 .= "<th>".nomDiaCurt($i,$this->lang)."</th>";
					}
				$cal00 .= "</tr>";
			}
			$control_dies = 1; // Contador dels dies del mes
			$control_setmana = 1; // Contador de setmantes
			
			while($control_setmana <= $setmanes_mes){
				$cal00 .= "<tr id='dies_setmana'>";
				
				// while($control_dia_setmana <= 7){
				for($d=1; $d<=7; $d++){
					$dia_selec = 'no_dia'; // Classe per defecte
					
					if($control_setmana == 1 and $d < $dia_setmana_inici){
						// Omplim els espais buits de la primera setmana, si n'hi ha
						$diaImp = "";
					}else if($control_setmana == $setmanes_mes and $control_dies > $dies_del_mes){
						// Omplim els espais buits de final de mes, si n'hi ha
						$diaImp = "";
					}else{
						// Posem els dies
						$diaImp = $control_dies;
						// Mirem quin tipus de dia escollim la seva classe
						$data_del_dia = $anyA."-".$mesA."-".$control_dies;
						if($data_del_dia == $this->data){
							$dia_selec = 'avui';
						}else if($d == 6){
							$dia_selec = 'es_dissabte';
						}else if($d == 7){
							$dia_selec = 'es_diumenge';
						}else{
							$dia_selec = 'dia';
						}
						if($data_del_dia == $dataSelec){
							$dia_selec .= ' seleccionat';
						}
						$control_dies++; // Sumem un dia
					}
					$cal00 .= "<td class='".$dia_selec."' align='right'>";
					if($this->nav){
						$cal00 .= "<a href='".$this->path.$anyA."-".$mesA."-".$diaImp."'>".$diaImp."</a>";
					}else{
						$cal00 .= $diaImp;
					}
					$cal00 .= "</td>";
					
				}
				$control_setmana++;
				$cal00 .= "</tr>";
			}
		$cal00 .= "</table>";
		
		return $cal00;
	}
}

// TRADUCCIONS
	// Funció que retorna el dia de la setmana en format curt
	function traduccio($tipus,$lang){
		if($lang == 'ca'){
			$dies = array('Dilluns','Dimarts','Dimecres','Dijous','Divendres','Dissabte','Diumenge');
			$diesCurt = array('Dl','Da','Dc','Dj','Dv','Ds','Dg');
			$mesos = array('Gener','Febrer','Març','Abril','Maig','Juny','Juliol','Agost','Setembre','Octubre','Novembre','Desembre');
		}else if($lang == 'es'){
			$dies = array('Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');
			$diesCurt = array('Lu','Ma','Mi','Ju','Vi','Sa','Do');
			$mesos = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
		}else if($lang == 'it'){
			$dies = array('Lunedì','Martedì','Mercoledì','Giovedì','Vernedì','Sabato','Domenica');
			$diesCurt = array('Lu','Ma','Me','Gi','Ve','Sa','Do');
			$mesos = array('Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Iuglio','Agosto','Settembre','Ottobre','Novembre','Dicembre');
		}else if($lang == 'ru'){
			$dies = array('Понедельник','Вторник','Среда','Четверг','Пятница','Суббота','Воскресенье');
			$diesCurt = array('Пн','Вт','Ср','Чт','Пт','Сб','Вс');
			$mesos = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
		}else{
			$dies = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
			$diesCurt = array('Mo','Tu','We','Th','Fr','Sa','Su');
			$mesos = array('January','February','March','April','May','June','July','August','September','October','November','December');
		}
		if($tipus=='dies'){
			return $diesCurt;
		}else if($tipus=='diaCurt'){
			return $diesCurt;
		}else if($tipus=='mesos'){
			return $mesos;
		}
	}


	// Funció que retorna el dia de la setmana en format curt
	function nomDiaCurt($d,$lang){
		return traduccio('diaCurt',$lang)[$d];
	}

	// Funció que retorna el nom del mes
	function nomMes($m,$lang){
		return traduccio('mesos',$lang)[$m];
	}
// fi Traduccions


// Funció per saber si és any de traspàs // Boolean: true / false
function anyDeTraspas($any){
	/* Són any de traspàs aquells que són múltiples de 4.
		Excepte els anys seculars (múltiples de 100) que no siguin múltiples de 400.
		Exemple: L'any 2012 és de traspàs perquè és divisible per 4
		L'any 2100 és divisible per 4, però no és de traspàs perquè no és un any secular divisible per 400. */
	if($any % 4 == 0){
		$secular = substr($any, -2);
		if($secular == '00' and $any % 400 != 0){
			return false;
		}else{
			return true;
		}
	}else{
		return false;
	}
}


// Funció per saber quants dies te el mes
function numeroDiesMes($mes,$any){
	$dies = 0;
	if( $mes == 1 or $mes == 3 or $mes == 5 or $mes == 7 or $mes == 8 or $mes == 10 or $mes == 12 ){
		$dies = 31;
	}else if( $mes == 4 or $mes == 6 or $mes == 9 or $mes == 11 ){
		$dies = 30;
	}else if($mes == 2){
		$any_de_traspas = anyDeTraspas($any);
		if($any_de_traspas){
			$dies = 29;
		}else{
			$dies = 28;
		}
	}
	return $dies;
}


// Funció per saber el dia de la setmana
function diaDeLaSetmana($data){
	list($any,$mes,$dia) = explode('-',$data); // Guardem la data en variables separades
	$dies_passats = 0; // Dies que han passat des de la data 1-1-1
		/* El calendari Gregorià funciona per cicles iguals de 400 anys.
			L'1 de gener de l'any 1 és dilluns.
			De manera que l'1 de gener de l'any 401 també és dilluns. Igual per l'any 801, 1201, 1601, 2001...
			*/
	// Contem quants anys hi ha des de l'inici del cicle de 400 anys
	$any400 = $any % 400;
		if($any400 == 0){
			$any400 = 400;
		}
	$anys_falten = $any400 - 1;
	
	// Contem quants dies han passat des de 1-1-1 fins l'any consultat
	for($i=1; $i<=$anys_falten; $i++){
		// Mirem si és any de traspàs o no per sumar els dies corresponents
		$any_traspas = anyDeTraspas($i);
		if($any_traspas){
			$dies_passats = $dies_passats + 366; // Sumem un any de traspàs
		}else{
			$dies_passats = $dies_passats + 365; // Sumem un any
		}
	}
	
	// Contem els dies que falten fins els mes consultat
	for($i=1; $i<$mes; $i++){
		$num_dies_mes = numeroDiesMes($i,$any400);
		$dies_passats = $dies_passats + $num_dies_mes;
	}
	
	// Contem els dies que falten fins el dia consultat
	for($i=0; $i<$dia; $i++){
		$dies_passats++;
	}
	
	// Mirem a quin dia de la setmana correspon
	$dia_setmana = $dies_passats % 7;
		if($dia_setmana == 0){  // Si és 0 posem 7 perquè és diumgenge
			$dia_setmana = 7;
		}
	return $dia_setmana;
}
?>
