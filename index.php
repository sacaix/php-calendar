<?php
// Calendari°° PHP
// Problem Y2K38 solved!
// arnAu bellavista
// @sacaix
// kipple.net

include("calendar00.php");
	
// Agafem la data per GET si se n'especifica
if(isset($_GET['data'])){
	$dataGET = trim($_GET['data']);
	$dex = explode('-',$dataGET);
	if(count($dex)==3 or count($dex)==2){
		$any = $dex[0];
		if(empty($dex[1])){
			$mes = 0;
		}else{
			$mes = $dex[1];
		}
		if(empty($dex[2])){
			$dia = 0;
		}else{
			$dia = $dex[2];
		}
	}
}else{
	$dia = 0;
}
?>

<!DOCTYPE html>
<html>
<head>

	<link rel="shortcut icon" href="icona.ico" /> 
	 
	<meta http-equiv="Pragma" content="no-cache"> 
	<meta http-equiv="cache-control" value="no-cache, no-store, must-revalidate"> 
	<meta http-equiv="Expires" content="Mon, 01 Jan 1990 00:00:01 GMT"> 

	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta charset="UTF-8">
	<meta name="author" content="sacaix-es melirsacma" />

	<!-- Responsive -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title>Calendar°°</title>
	<meta name='title' content='Calendar°°' />
	<meta name='description' content='PHP Calendar' />
		
	<meta name="language" content="ca" />

	<!-- <base href="http://<?php //echo $_SERVER['HTTP_HOST']; ?>" /> -->
	<!-- <base href="http://127.0.0.1/00calendari/" /> -->

	<link rel="stylesheet" type="text/css" href="estils.css" />
	<link rel="stylesheet" type="text/css" href="calendar00.css" />

</head>





<body>
<div class='general'>
		
	<main>
	
		<h1>Calendar°°</h1>
		<hr>
	
		<article>
			<div>
				<p>
					<strong>PHP + HTML + CSS</strong>
					<br>
					Gregorian calendar
					<br>
					Problem Y2K38 solved!
					<br>
					Doesn't use <em>strtotime</em> or the obsolete <em>mktime</em>
					<br>
					arnAu bellavista (@sacaix)
					<br>
					<a href='https://kipple.net' target='_blank'>www.kipple.net</a>
				</p>
			</div>
			
			<div>
				<p>
					<h2>Languages</h2>
					Calendar°° has the following languages:
					<br>
					[ca] Catalan
					<br>
					[en] English (by default)
					<br>
					[es] Spanish
					<br>
					[it] Italian
					<br>
					[ru] Russian
				</p>
			</div>
			
			<div>
				
			</div>
		</article>
		
		
		
		<hr>
	
	
	
		
		<article>
			<div>
				<p>
					<h2>Actual month and date</h2>
					<?php
					$cal01 = new Calendar00();
					echo $cal01->month();
					?>
				</p>
				<p>
				Default presentation: actual month in english
<xmp class='codi'>$cal01 = new Calendar00();
echo $cal01->month();
</xmp>
				</p>
			</div>
			
			<div>
				<p>
					<h2>Defined month</h2>
					<?php
					$cal02 = new Calendar00();
						$cal02->set_year(2023);
						$cal02->set_month(1);
						$cal02->set_day(11);
						$cal02->set_week(true);
						$cal02->set_lang('ru');
					echo $cal02->month();
					?>
				</p>
				<p>
				Specific month and date in russian
<xmp class='codi'>$cal02 = new Calendar00();
$cal02->set_year(2023);
$cal02->set_month(1);
$cal02->set_day(11);
$cal02->set_week(true);
$cal02->set_lang('ru');
echo $cal02->month();
</xmp>
				</p>
			</div>
			
			<div>
				<p>
					<h2>Navigable month</h2>
					<?php
					$cal03 = new Calendar00();
						if(!empty($any)){
							$cal03->set_year($any);
						}
						if(!empty($mes)){
							$cal03->set_month($mes);
						}
						if(!empty($dia)){
							$cal03->set_day($dia);
						}
						$cal03->set_week(true);
						$cal03->set_nav(true);
						$cal03->set_path('index.php?data=');
						$cal03->set_lang('ca');
					echo $cal03->month();
				?>
				</p>
				<p>
					Customized month with all the possible parameters:
					<br>
					- Specific year, month and day (current month displayed if no date is specified by GET)
					<br>
					- Show the days of the week
					<br>
					- Navigable calendar with the url path
					<br>
					- Custom language, in this case catalan
<xmp class='codi'>$cal03 = new Calendar00();
$cal03->set_year(2024);
$cal03->set_month(12);
$cal03->set_day(11);
$cal03->set_week(true);
$cal03->set_nav(true);
$cal03->set_path('index.php?data=');
$cal03->set_lang('ca');
echo $cal03->month();
</xmp>
				</p>
			</div>
			
		</article>
		
		
		
		
		<hr>
		
		
		
		
		<article>
			<div>
				<h2>Entire year</h2>
				<p>
				Show entire specific year
<xmp class='codi'>$calYear = new Calendar00();
$calYear->set_year(1992);
echo $calYear->year();
</xmp>
				</p>
				<p>
					<?php
					$calYear = new Calendar00();
					$calYear->set_year(1992);
					echo $calYear->year();
					?>
				</p>
			</div>
			
			<div>
				<h2>12 months</h2>
				<p>
				Show 12 months from current date
<xmp class='codi'>$calMonths = new Calendar00();
echo $calMonths->months(12);
</xmp>
				</p>
				<p>
					<?php
					$calMonths = new Calendar00();
					echo $calMonths->months(12);
					?>
				</p>
			</div>
			
			<div>
				<h2>4 months</h2>
				<p>
				Show 4 months from specific date in italian
<xmp class='codi'>$cal4m = new Calendar00();
$cal4m->set_year(2020);
$cal4m->set_month(10);
$cal4m->set_lang('it');
echo $cal4m->months(4);
</xmp>
				</p>
				<p>
					<?php
					$cal4m = new Calendar00();
					$cal4m->set_year(2020);
					$cal4m->set_month(10);
					$cal4m->set_lang('it');
					echo $cal4m->months(4);
					?>
				</p>
			</div>
		</article>
		
		
	<main>
</div>
</body>
</html>
